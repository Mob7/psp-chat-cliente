import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by sergio on 05/03/2016.
 */
public class Chat {
    private JPanel panel1;
    private JTextField tfMensaje;
    private JTextArea taChat;
    private JList listAmigos;
    private JButton btApagado;
    private JButton btEncendido;
    private DefaultListModel modelAmigos;
    private Socket socket;
    private PrintWriter salida;
    private BufferedReader entrada;
    private boolean conectado;
    private static final int PUERTO = 5555;
    private ArrayList<String> listaDeBaneos;
    private String nick;

    public Chat() {
        tfMensaje.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    enviarMensaje();
            }
        });

        btEncendido.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "El servidor está conectado");
            }
        });

        btApagado.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "El servidor está desconectado");
            }
        });

        inicializar();
    }

    public JMenuBar getMenuBar() {

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Chat");
        menuBar.add(menu);
        JMenuItem menuItem = new JMenuItem("Conectar");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                conectar();
            }
        });
        menu.add(menuItem);
        menuItem = new JMenuItem("Desconectar");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                desconectar();
            }
        });
        menu.add(menuItem);
        menuItem = new JMenuItem("Salir");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                salir();
            }
        });
        menu.add(menuItem);

        return menuBar;
    }

    private void inicializar() {
        modelAmigos = new DefaultListModel<>();
        listAmigos.setModel(modelAmigos);
        listaDeBaneos = new ArrayList<String>();

        tfMensaje.requestFocus();
    }

    private void conectar() {

        final Conectar conecta = new Conectar();

        if (conecta.mostrarDialogo() == Constantes.Accion.CANCELAR)
            return;

        // Ejecuta el método en el EDT (Event Dispatch Thread)
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String host = conecta.getHost();
                conectarServidor(host);
            }
        });
    }

    private void conectarServidor(String servidor) {

        try {
            socket = new Socket(servidor, PUERTO);
            salida = new PrintWriter(socket.getOutputStream(), true);
            entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            conectado = true;
            btEncendido.setEnabled(true);
            btApagado.setEnabled(false);

            Thread hiloRecibir = new Thread(new Runnable() {
                public void run() {
                    while (conectado) {
                        try {
                            if (socket.isClosed()) {
                                conectado = false;
                                break;
                            }
                            String mensaje = entrada.readLine();
                            if (mensaje == null)
                                continue;

                            int indice = 0;
                            if (mensaje.startsWith("/server")) {
                                indice = mensaje.indexOf(" ");
                                taChat.append("** " + mensaje.substring(indice + 1) + " **\n");
                            } else if (mensaje.startsWith("/users")) {
                                boolean encontrado=false;
                                indice = mensaje.indexOf(" ", 7);
                                String nick = mensaje.substring(7, indice);
                                for(int x=0; x<listaDeBaneos.size(); x++){
                                    if(nick.equals(listaDeBaneos.get(x))){
                                        taChat.append("El usuario que ha escrito está ignorado. No puedes ver sus mensajes\n");
                                        encontrado=true;
                                    }
                                }
                                if(encontrado==false){
                                    taChat.append(" " + nick + ": ");
                                    taChat.append(mensaje.substring(indice + 1) + "\n");
                                }
                            } else if (mensaje.startsWith("/nicks")) {
                                String[] nicks = mensaje.split(",");
                                modelAmigos.clear();
                                for (int i = 1; i < nicks.length; i++) {
                                    modelAmigos.addElement(nicks[i]);
                                }
                            }  else if(mensaje.startsWith("/ban")) {
                                String[] palabras = mensaje.split(" ");
                                String nickBaneado = palabras[1];
                                for (int i = 0; i < modelAmigos.size(); i++) {
                                    if (modelAmigos.get(i).equals(nickBaneado)) {
                                        taChat.append(nickBaneado+" ignorado\n");
                                        modelAmigos.set(i, modelAmigos.get(i) + " está ignorado");
                                        listaDeBaneos.add(nickBaneado);
                                    }
                                }
                            } else if(mensaje.startsWith("/sobrenombre")) {
                                String[] palabras = mensaje.split(" ");
                                nick = palabras[1];
                            } else if (mensaje.startsWith("/private")){
                                String[] palabras = mensaje.split(" ");
                                String recibe = palabras[2];
                                recibe = recibe.substring(0, recibe.length() - 1);
                                for (int i = 0; i < listaDeBaneos.size(); i++) {
                                    if (listaDeBaneos.get(i).equals(palabras[1])) {
                                        taChat.append("El usuario " + palabras[1] + " te ha escrito un mensaje privado pero está ignorado\n");
                                        break;
                                    }
                                }
                                indice = mensaje.indexOf(":");

                                if (recibe.equals(nick)){
                                    taChat.append(nick + " ¡has recibido un mensaje privado de " + palabras[1] + "!. El mensaje es el siguiente: \n");
                                    taChat.append("\t"+mensaje.substring(indice+1)+"\n");
                                } else if (palabras[1].equals(nick)){
                                    taChat.append("Mensaje privado enviado correctamente a "+recibe+"\n");
                                }
                            } else if(mensaje.startsWith("/quit")){
                                desconectar();
                            }
                        } catch (SocketException se) {
                            desconectar();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                }
            });
            hiloRecibir.start();

        } catch (UnknownHostException uhe) {
            uhe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void enviarMensaje() {

        String mensaje = tfMensaje.getText();
        salida.println(mensaje);

        tfMensaje.setText("");
    }

    private void desconectar() {
        try {
            salida.println("/quit");
            taChat.append("Has sido desconectado");
            conectado = false;
            btEncendido.setEnabled(false);
            btApagado.setEnabled(true);
            socket.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void salir() {
        System.exit(0);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Chat");
        Chat Chat = new Chat();
        frame.setJMenuBar(Chat.getMenuBar());
        frame.setContentPane(Chat.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setSize(600, 600);
    }
}
